import unittest

import contextlib
import io

import hello


class TestGreet(unittest.TestCase):
    def test_greet(self):
        self.assertEqual(hello.greet('test'), 'Hello, test')
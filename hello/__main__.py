import sys

# default name is 'World'.
# Defaults are great!!! Author: Zach,  Email: whosyourdaddy@joe.com
name = sys.argv[1] if len(sys.argv) > 1 else 'World'
print('hello ' + name)